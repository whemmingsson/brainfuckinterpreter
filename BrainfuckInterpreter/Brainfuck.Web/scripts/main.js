﻿var code = "";
var validChars = "+-><[].,";
var markerPosition = -1;
var numberOfChars = 0;

var INPUT_KEY = {
    Return: 13,
    Space: 32,
    Backspace: 8,
    RightArrow: 39,
    LeftArrow:37
};

$(function () {
    $("#text-input").focus();

    $("#text-input").on("keypress ", function (e) {
        var char = String.fromCharCode(e.which);
        var span;
        if (isValidCharacter(char)) {
            var charClass = getCharacterClass(char);
            span = getSpanWithClass(char, charClass);
                 
            if (markerPosition > 0) {
                $("#code span").eq(markerPosition).after(span);
            }
            else {
                $("#code").append(span);
            }

            markerPosition++;
            numberOfChars++;
        }
        else if (e.which == INPUT_KEY.Return) {
            $("#code").append(getSpan("<br/>"));
            markerPosition++;
            numberOfChars++;
        }
        else if (e.which == INPUT_KEY.Space) {
            $("#code").append(getSpan("&nbsp;"));
            markerPosition++;
            numberOfChars++;
        }
        else if (e.which == INPUT_KEY.Backspace) {
            $("#code span").eq(markerPosition).remove();
            markerPosition--;
            numberOfChars--;
        }
       
        updateMarker();
    });

    $("#text-input").on("keydown ", function (e) {
        if (e.which == INPUT_KEY.RightArrow && markerPosition < numberOfChars-1) {
            markerPosition++;
        }
        else if (e.which == INPUT_KEY.LeftArrow && markerPosition > 0) {
            markerPosition--;
        }
    });


    $("#text-input").on("keyup ", function (e) {
        $(this).val('');
    });


    $("#code span").on("click", function (e) {
        alert("clickey");
        markerPosition = $("#code span").indexOf(this);
        updateMarker();
    });
});

var getSpanWithClass = function(v,c){
    return "<span class='" + c + "'>" + v + "</span>";
}

var getSpan = function (v) {
    return "<span>" + v + "</span>";
}

var isValidCharacter = function (c) {
    return validChars.indexOf(c) != -1;
}

var updateMarker = function () {
    $("#code span").removeClass("current");
    $("#code span").eq(markerPosition).addClass("current");
}

var getCharacterClass = function (c) {
    if (c == '+') {
        return "value-up";
    }
    if (c == '-') {
        return "value-down";
    }
    if (c == '>') {
        return "shift-up";
    }
    if (c == '<') {
        return "shift-down";
    }
    if (c == '.') {
        return "write";
    }
    if (c == ',') {
        return "read";
    }
    if (c == '[') {
        return "loop-start";
    }
    if (c == ']') {
        return "loop-end";
    }

    return "";
}
