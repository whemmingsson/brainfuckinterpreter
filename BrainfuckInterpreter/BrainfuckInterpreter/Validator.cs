﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interpreter
{
    public static class Validator
    {
        public static bool ValidateBrackets(string code)
        {
            var stack = new Stack<char>();

            foreach(var c in code)
            {
                if(c == InstructionCharacters.LoopStart)
                {
                    stack.Push(c);
                }

                if(c == InstructionCharacters.LoopStop)
                {
                    stack.Pop();
                }
            }

            return !stack.Any();
        }
    }
}
