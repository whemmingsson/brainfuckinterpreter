﻿namespace Interpreter
{
    public class Memory
    {
        private byte[] _memory;
        private uint _pointer;

        public Memory()
        {
            _pointer = 0;
            _memory = new byte[Constants.MemorySize];
            InitMemory();            
        }

        private void InitMemory()
        {
            for(var i = 0; i < _memory.Length; i++)
            {
                _memory[i] = Constants.InitalMemoryCellValue;
            }
        }

        public void IncreasePointer()
        {
            _pointer++;
        }

        public void DecreasePointer()
        {
            _pointer--;
        }

        public void IncreaseValue()
        {
            _memory[_pointer]++;
        }

        public void DecreaseValue()
        {
            _memory[_pointer]--;
        }

        public char ReadChar()
        {
            return (char)_memory[_pointer];
        }

        public byte ReadValue()
        {
            return _memory[_pointer];
        }

        public void Write(char value)
        {
            _memory[_pointer] = (byte)value;
        }      
    }
}
