﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Interpreter
{
    public class Interpreter
    {
        private char[] _code;
        private Memory _memory;
        private int _currentInstruction;
        private Stack<int> _jumpTo;

        public Interpreter()
        {
            _memory = new Memory();
            _jumpTo = new Stack<int>();
        }   

        public void LoadCode(string code)
        {
            _code = code.ToArray();
        }

        public Action<char> WriteAction { set; get; }
        public Func<char> ReadAction { set; get; }

        public void Run()
        {
            _currentInstruction = 0;

            do
            {
                var instructionCharacter = GetCurrentInstructionCharacter();
                RunInstruction(instructionCharacter);
               _currentInstruction++;
            }
            while (ShouldRun());
        }

        private Dictionary<char, Action> Mappings
        {
            get
            {
                return new Dictionary<char, Action>()
                {
                    {InstructionCharacters.DecreasePointer, () => _memory.DecreasePointer() },
                    {InstructionCharacters.IncreasePointer, () => _memory.IncreasePointer() },
                    {InstructionCharacters.DecreaseValue, () => _memory.DecreaseValue() },
                    {InstructionCharacters.IncreaseValue, () => _memory.IncreaseValue() },
                    {InstructionCharacters.LoopStart, StartLoop },
                    {InstructionCharacters.LoopStop, StopLoop },
                    {InstructionCharacters.WriteOutput, WriteToOutput },
                    {InstructionCharacters.ReadInput, ReadFromInput },
                };
            }
        }

        private char GetCurrentInstructionCharacter()
        {
            return _code[_currentInstruction];
        }

        private void RunInstruction(char character)
        {
            if(Mappings.ContainsKey(character))
            {
                Mappings[character]();
            }
        }

        private bool ShouldRun()
        {
            return _currentInstruction < _code.Length;
        }

        private void StartLoop()
        {
            _jumpTo.Push(_currentInstruction + 1);
        }

        private void StopLoop()
        {
            // if the current value of the cell is 0, then remove the last jump
            if(_memory.ReadValue() == Constants.InitalMemoryCellValue)
            {
                if (_jumpTo.Count > 0)
                {
                    _jumpTo.Pop();
                }
                else
                {
                    throw new Exception("Error: loop brackets do not match up!");
                }
            }
            // else continue the loop by jumping to the current loop start position
            else
            {
                _currentInstruction = _jumpTo.Peek() - 1;
            }
        }
        
        private void WriteToOutput()
        {
            var charValue = _memory.ReadChar();
            WriteAction(charValue); // Callback
        }
        
        private void ReadFromInput()
        {
            var charValue = ReadAction(); // Callback
            _memory.Write(charValue); 
        }            
    }
}
