﻿namespace Interpreter
{
    internal static class InstructionCharacters
    {
        public const char IncreaseValue = '+';
        public const char DecreaseValue = '-';
        public const char IncreasePointer = '>';
        public const char DecreasePointer = '<';
        public const char LoopStart = '[';
        public const char LoopStop = ']';
        public const char WriteOutput = '.'; // Write to output (console)
        public const char ReadInput = ','; // Read from input (user)
    }
}
