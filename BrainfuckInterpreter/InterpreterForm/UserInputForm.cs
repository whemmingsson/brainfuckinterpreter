﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InterpreterForm
{
    public partial class UserInputForm : Form
    {
        private char value;
        public UserInputForm()
        {
            InitializeComponent();
        }

        internal char GetValue()
        {
            return value;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (txtCharValue.Text.Length == 1)
            {
                value = txtCharValue.Text[0];
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }
    }
}
