﻿using Interpreter;
using System;
using System.Windows.Forms;

namespace InterpreterForm
{
    public partial class InterpreterForm : Form
    {
        public InterpreterForm()
        {
            InitializeComponent();
        }

        private void btnRun_Click(object sender, EventArgs e)
        {
            var interpreter = new Interpreter.Interpreter()
            {
                WriteAction = Write,
                ReadAction = Read
            };

            interpreter.LoadCode(txtCode.Text);

            try
            {
                if (Validator.ValidateBrackets(txtCode.Text))
                {
                    interpreter.Run();
                }
                else
                {
                    MessageBox.Show("Brackets do not match up", "Validation error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Write(char c)
        {
            txtOutput.Text += c;
        }

        private char Read()
        {
            var userInputForm = new UserInputForm();

            if(userInputForm.ShowDialog() == DialogResult.OK)
            {
                return userInputForm.GetValue();
            }

            return '0';                     
        }

        private void btnClearOutput_Click(object sender, EventArgs e)
        {
            txtOutput.Clear();
        }
    }
}
